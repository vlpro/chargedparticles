﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargedParticlesManager : MonoBehaviour {

    public ComputeShader particlesForcesShader;

    ComputeBuffer particlesBuffer;
    ComputeBuffer forcesBuffer;
    int _kernel;
    int cachedComputeBufferSize = -1;

    Vector3[] forces;
    Vector4[] particlesData;
    ChargedParticle[] particles;
    int particlesNum;
    Rigidbody[] rigidBodies;

    void Start()
    {
        _kernel = particlesForcesShader.FindKernel("CSMain");

        particles = GetComponentsInChildren<ChargedParticle>();
        particlesNum = particles.Length;
        particlesBuffer = new ComputeBuffer(particlesNum, 4 * 4);
        forcesBuffer = new ComputeBuffer(particlesNum, 3 * 4);
        forces = new Vector3[particlesNum];
        particlesData = new Vector4[particlesNum];

        particlesForcesShader.SetBuffer(_kernel, "particles", particlesBuffer);
        particlesForcesShader.SetBuffer(_kernel, "forces", forcesBuffer);
        particlesForcesShader.SetInt("particlesNumber", particlesNum);

        cachedComputeBufferSize = particlesNum;

        rigidBodies = new Rigidbody[particlesNum];
        for (int i = 0; i < particlesNum; i++)
        {
            rigidBodies[i] = particles[i].GetComponent<Rigidbody>();
        }
    }

    void UpdateComputeBuffer()
    {

        for (int i = 0; i < particlesNum; i++)
        {
            var particlePosition = particles[i].transform.position;
            particlesData[i] = new Vector4(particlePosition.x, particlePosition.y, particlePosition.z, particles[i].charge);//

        }
        particlesBuffer.SetData(particlesData);
    }

    void FixedUpdate()
    {
        UpdateComputeBuffer();
        particlesForcesShader.Dispatch(_kernel, cachedComputeBufferSize, 1, 1);
        forcesBuffer.GetData(forces);
        for (int i = 0; i < particles.Length; i++)
        {
            rigidBodies[i].AddForce(forces[i]);
        }
    }

    void OnDisable()
    {
        if (particlesBuffer != null) particlesBuffer.Release();
        particlesBuffer = null;

        if (forcesBuffer != null) forcesBuffer.Release();
        forcesBuffer = null;
    }
}


