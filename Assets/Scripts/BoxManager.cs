﻿using UnityEngine;

[ExecuteInEditMode]
public class BoxManager : MonoBehaviour
{
    public Vector3 size;
    public PhysicMaterial boxMaterial;
    GameObject FindOrCreatePlane(string name, Vector3 position, Vector3 rotation, Vector3 scale)
    {
        GameObject plane = transform.Find(name) != null ? transform.Find(name).gameObject : null;
        if (plane == null)
        {
            plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
            plane.name = name;
            plane.transform.parent = transform;
            plane.AddComponent<BoxCollider>();
        }
        var boxCollider = plane.GetComponent<BoxCollider>();
        boxCollider.center = new Vector3(0, -2.0f, 0);
        boxCollider.size = new Vector3(15.0f, 4.0f, 15.0f);
        boxCollider.material = boxMaterial;

        plane.transform.position = position;
        plane.transform.rotation = Quaternion.Euler(rotation.x, rotation.y, rotation.z);
        plane.transform.localScale = scale;
        return plane;
    }
    
    void OnValidate()
    {
        FindOrCreatePlane(
            "Top", 
            new Vector3(0, size.y, 0), 
            new Vector3(180.0f, 0.0f, 0.0f), 
            new Vector3(size.x, 10.0f, size.z) * 0.1f
        );
        FindOrCreatePlane(
            "Bottom", 
            new Vector3(0, 0, 0), new Vector3(0.0f, 0.0f, 0.0f), 
            new Vector3(size.x, 10.0f, size.z) * 0.1f
        );
        FindOrCreatePlane(
            "Left", 
            new Vector3(-size.x / 2.0f, size.y / 2.0f, 0), 
            new Vector3(0.0f, 0.0f, -90.0f), 
            new Vector3(size.y, 10.0f, size.z) * 0.1f
        );
        FindOrCreatePlane(
            "Right", 
            new Vector3(size.x / 2.0f, size.y / 2.0f, 0), 
            new Vector3(0.0f, 0.0f, 90.0f), 
            new Vector3(size.y, 10.0f, size.z) * 0.1f
        );
        FindOrCreatePlane(
            "Front", 
            new Vector3(0.0f, size.y / 2.0f, -size.z / 2), 
            new Vector3(90.0f, 0.0f, 0.0f), 
            new Vector3(size.x, 10.0f, size.y) * 0.1f
        );
        FindOrCreatePlane(
            "Back", 
            new Vector3(0.0f, size.y / 2.0f, size.z / 2), 
            new Vector3(-90.0f, 0.0f, 0.0f), 
            new Vector3(size.x, 10.0f, size.y) * 0.1f
        );
    }
}
