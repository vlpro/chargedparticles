﻿using UnityEngine;
using System.Collections;

public class FlyingCamera : MonoBehaviour
{
    public float cameraSensitivity = 90;
    public float climbSpeed = 4;
    public float normalMoveSpeed = 10;
    public float slowMoveFactor = 0.25f;
    public float fastMoveFactor = 3;

    private float rotationX = 0.0f;
    private float rotationY = 0.0f;

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        var rot = transform.rotation.eulerAngles;
        rotationX = rot.y;
        rotationY = -rot.x;
    }

    void Update()
    {
        rotationX += Input.GetAxis("Mouse X") * cameraSensitivity * Time.deltaTime;
        rotationY += Input.GetAxis("Mouse Y") * cameraSensitivity * Time.deltaTime;
        rotationY = Mathf.Clamp(rotationY, -90, 90);

        transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);
        transform.localRotation *= Quaternion.AngleAxis(rotationY, Vector3.left);

        float speed;

        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            speed = normalMoveSpeed * fastMoveFactor;
        }
        else if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        {
            speed = normalMoveSpeed * slowMoveFactor;
        }
        else
        {
            speed = normalMoveSpeed;
        }

        if (Input.GetKey(KeyCode.Q))
        {
            transform.position += transform.up * speed * Time.deltaTime;
            speed /= 2.0f;
        } else
        if (Input.GetKey(KeyCode.E)) {
            transform.position -= transform.up * speed * Time.deltaTime;
            speed /= 2.0f;
        }


        transform.position += transform.forward * speed * Input.GetAxis("Vertical") * Time.deltaTime;
        transform.position += transform.right * speed * Input.GetAxis("Horizontal") * Time.deltaTime;


        if (Input.GetKeyDown(KeyCode.End))
        {
            Cursor.lockState = (Cursor.lockState == CursorLockMode.None) ? CursorLockMode.Locked : CursorLockMode.None;
            Cursor.visible = (Cursor.visible == false) ? true : false;
        }
    }
}
