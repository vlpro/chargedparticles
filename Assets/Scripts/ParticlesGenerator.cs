﻿using UnityEngine;

[System.Serializable]
public class ParticleType {
	public int amount;
	public GameObject prefab;
}

public class ParticlesGenerator : MonoBehaviour {
	public enum GeneratorType
	{
		LayersHorizontal,
		Random
	};
	public GeneratorType generatorType;
    public float boundingBoxSize = 1.0f;
	public ParticleType[] particleTypes;
   
}